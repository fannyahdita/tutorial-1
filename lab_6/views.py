from django.shortcuts import render

# Create your views here.
name = 'Fannyah'

def index(request):
    response = {'author' : name}
    html = "lab_6/lab_6.html"
    return render(request, html, response)
