  // FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '1872627589716999',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    FB.getLoginStatus(function(response){
        if(response.status == 'connected'){
          render(true);
        } else {
          facebookLogin();
        }
    })

  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  const render = loginFlag => {
    if (loginFlag) {
      getUserData(user => {
        $('#lab8').html(
          '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" /> <br>'+
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' + '<br>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<input id="postInput" type="text" class="post" placeholder="Whats on your mind?" />' + '<br>' +
          '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' + '<br>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button>' + '<br>'
        );


        getUserFeed(feed => {
          feed.data.map(value => {
             if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story + '</h2>' +
                '</div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h2>' + value.story + '</h2>' +
                '</div>'
              );
            }
          });
        });
      });
    } else {
      $('#lab8').html('<button class="login-button">Login</button>');
    }
  };

  const facebookLogin = () => {
    Fb.login(function(response){
      console.log(response);
      document.location.reload();
    }, {scope : 'public_profile,email,user_about_me,user_birthday,publish_actions,user_posts'},
    {auth_type : 'reauthenticate'} 
    );
  };

  // $('#login-button').click(facebookLogin);

  const facebookLogout = () => {
    FB.getLoginStatus(function(response){
      if(response.status === 'connected'){
        FB.logout();
        document.location.reload();
      } 
    });
  };
  
  const getUserData = (fun) => {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api(
                '/me?fields=id,cover,picture,name,about,birthday,email,gender,first_name',
                'GET',
                function(response){
                    console.log(response);
                    fun(response); 
                });
            }
         });
    };

  const getUserFeed = (fun) => {
    FB.getLoginStatus(function(response){
        if(response.status === 'connected'){
            FB.api('/me/feed/', 'GET', function(response){
              console.log(response);
              fun(response);
            }
          );
        }
      });
    };

  const postFeed = (messageText) => {
     FB.api(
        "/me/feed",
        "POST",
        {
            "message": messageText
        },
        function (response) {
          if (response && !response.error) {
              console.log('POST ID: ' + response.id);
              render(false);
              render(true);
          }else{
              alert('Error occured');
          }
        }
    );

  };

  const postStatus = () => {
    const message = $('#postInput').val();
    alert(message);
    postFeed(message);
  };
