from django.shortcuts import render

# Create your views here.
name = 'Fannyah'

def index(request):
    response = {'author' : name}
    html = "lab_8/lab_8.html"
    return render(request, html, response)
